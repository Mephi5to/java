package ru.nsu.fit.Seleznev.server.Handlers;

import ru.nsu.fit.Seleznev.server.HttpRequest;
import ru.nsu.fit.Seleznev.server.HttpResponse;

public interface HttpHandler {
    HttpResponse handle(HttpRequest request);
}
