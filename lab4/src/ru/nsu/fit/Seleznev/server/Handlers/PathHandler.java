package ru.nsu.fit.Seleznev.server.Handlers;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.HttpRequest;
import ru.nsu.fit.Seleznev.server.HttpResponse;

import java.io.File;
import java.nio.file.Paths;

public class PathHandler implements HttpHandler {

    @Override
    public HttpResponse handle(HttpRequest request) {
        HttpResponse response = new HttpResponse();

        try {
            String path = request.getPath();
            path = path.replace('/', File.separatorChar);
            File file = Paths.get(path).toFile();

            if (file.isDirectory()) {
                path += File.separatorChar + "index.html";
                file = Paths.get(path).toFile();
            }

            response.attachFile(file);
            response.setCodeResponse(HttpResponse.getOk());

        } catch (ServerException e) {
            response.setCodeResponse(e.err);
            return response;
        }

        return response;
    }
}
