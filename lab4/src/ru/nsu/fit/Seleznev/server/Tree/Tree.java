package ru.nsu.fit.Seleznev.server.Tree;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Handlers.HttpHandler;
import ru.nsu.fit.Seleznev.server.HttpRequest;
import ru.nsu.fit.Seleznev.server.HttpResponse;
import ru.nsu.fit.Seleznev.server.Matchers.SegmentMatcher;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Tree {
    private Node node;

    public Tree() {
    }

    public HttpHandler getHandler(List<String> segments, HttpRequest request) throws ServerException {
        Node cur = node;
        for (String segment : segments) {
            if (!cur.matcher.isMatching(segment, request)) {
                for (Node iter : cur.nodes) {
                    if (iter.matcher.isMatching(segment, request)) {
                        cur = iter;
                    }
                }
            }
        }

        if (cur.handler == null) {
            throw new ServerException(404);
        }

        return cur.handler;

    }

    public void register(List<SegmentMatcher> matchers, HttpHandler handler) {
        Node cur = node;
        boolean flag;
        for (SegmentMatcher matcher : matchers) {
            flag = false;
            if (cur == null) {
                cur = new Node(matcher);
                node = cur;
            } else {
                if (cur.matcher.equals(matcher)) {
                    continue;
                }
                if (cur.nodes.isEmpty()) {
                    Node node = new Node(matcher);
                    cur.nodes.addLast(node);
                    cur = cur.nodes.getFirst();
                } else {
                    for (Node iter : cur.nodes) {
                        if (iter.matcher.equals(matcher)) {
                            cur = iter;
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        Node node = new Node(matcher);
                        cur.nodes.addLast(node);
                        cur = node;
                    }
                }
            }
        }
        cur.handler = handler;
    }

    private class Node {
        private LinkedList<Node> nodes;
        private SegmentMatcher matcher;
        private HttpHandler handler;

        Node(SegmentMatcher matcher) {
            this.matcher = matcher;
            nodes = new  LinkedList<>();
            handler = null;
        }

    }
}
