package ru.nsu.fit.Seleznev.server;

import java.io.*;

public class HttpResponse {
    private static final String VERSION = "HTTP/1.1";
    private static final int OK = 200;
    public static final int INTERNAL_EXCEPTION = 500;

    private File file;
    private Integer codeResponse;
    private String[] headers;

    public HttpResponse() {
        codeResponse = 200;
    }

    public static int getOk() {
        return OK;
    }

    private void setHeaders(Integer length) {
        headers = new String[3];
        headers[0] = "Content-Type: text/html";
        headers[1] = "Content-Length: " + length.toString();
        headers[2] = "Connection: close";
    }

    public void setCodeResponse(Integer code) {
        codeResponse = code;
    }

    public void attachFile(File file) {
        this.file = file;
    }

    public void writeResponse(OutputStream out) {
        try {
            if (codeResponse != 200) {
                out.write(makeResponse().getBytes());
                out.flush();
                return;
            }

            FileInputStream documentStream = new FileInputStream(file);
            setHeaders(documentStream.available());
            String response = makeResponse();
            out.write(response.getBytes());
            int count;
            byte[] buffer = new byte[1024];
            while ((count = documentStream.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            documentStream.close();
            out.flush();
        }
        catch (IOException e) {
            codeResponse = 500;
            makeResponse();
        }
    }

    private String makeResponse() {
        String answer = VERSION + " " + getAnswer(codeResponse) + "\n";
        StringBuilder bldr = new StringBuilder(answer);

        if (codeResponse != OK) {
            bldr.append("Connection: close\n");
            return bldr.toString();
        }

        for (String header : headers) {
            bldr.append(header);
            bldr.append("\n");
        }
        bldr.append("\n");

        return bldr.toString();
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "200 OK";
            case 404:
                return "404 Not Found";
            case 405:
                return "405 Method Not Allowed";
            default:
                return "500 Internal Server Error";
        }
    }


}
