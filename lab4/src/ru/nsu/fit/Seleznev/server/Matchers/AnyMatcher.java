package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.HttpRequest;

public class AnyMatcher implements SegmentMatcher {
    private String name;

    AnyMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(SegmentMatcher other) {
        return other instanceof AnyMatcher;

    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }
}
