package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.HttpRequest;

public class IntMatcher implements SegmentMatcher {
    private String name;

    IntMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(SegmentMatcher other) {
        if (other instanceof IntMatcher) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        try {
            Integer.parseInt(str);
        }
        catch (NumberFormatException e) {
            return false;
        }

        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }
}
