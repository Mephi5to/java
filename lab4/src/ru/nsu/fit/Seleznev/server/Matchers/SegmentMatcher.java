package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.HttpRequest;

public interface SegmentMatcher {
    boolean equals(SegmentMatcher other);
    boolean isMatching(String str, HttpRequest request);
}
