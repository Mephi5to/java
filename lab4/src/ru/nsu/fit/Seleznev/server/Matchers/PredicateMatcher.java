package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.HttpRequest;

import java.util.function.Predicate;

public class PredicateMatcher implements SegmentMatcher {
    private Predicate predicate;
    private String name;

    PredicateMatcher(Predicate<String> predicate, String name) {
        this.predicate = predicate;
        this.name = name;
    }

    @Override
    public boolean equals(SegmentMatcher other) {
        if (other instanceof PredicateMatcher) {
            return predicate.equals(((PredicateMatcher) other).predicate);
        }

        return false;
    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }
}
