package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.HttpRequest;

public class StringMatcher implements SegmentMatcher {
    private String segment;
    private String name;

    public StringMatcher(String segment, String name) {
        this.segment = segment;
        this.name = name;
    }

    @Override
    public boolean equals(SegmentMatcher other) {
        return other instanceof StringMatcher && segment.equals(((StringMatcher) other).segment);

    }

    public boolean isMatching(String str, HttpRequest request) {
        if (!segment.equals(str)) {
            return false;
        }
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }
}
