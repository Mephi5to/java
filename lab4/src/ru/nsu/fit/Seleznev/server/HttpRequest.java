package ru.nsu.fit.Seleznev.server;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class HttpRequest {
    private static final String DEFAULT_FILES_DIR = "static";
    private String request;
    private Map<String, String> pathInfo;

    HttpRequest(InputStream inputStream) throws ServerException {
        pathInfo = new HashMap<>();
        initialize(new BufferedInputStream(inputStream));
    }

    private void initialize(BufferedInputStream inputStream) throws ServerException {
        boolean flag = false;
        byte[] array = new byte[1024];
        int err;
        try {
            for (int i = 0; !flag; ++i) {
                err = inputStream.read(array, i, 1);
                if (err < 0) {
                    throw new ServerException(500);
                }
                if (i < 2) {
                    continue;
                }
                if ((array[i] == '\n') && (array[i - 1] == '\n') || ((array[i - 1] == '\r') && (array[i - 2] == '\n'))) {
                    flag = true;
                }
            }
        } catch (IOException e) {
            throw new ServerException(HttpResponse.INTERNAL_EXCEPTION);
        }

        request = new String(array);
    }

    public void addInfo(String key, String value) {
        pathInfo.put(key, value);
    }

    public Map<String, String> getParameters() {
        String requestString = request.split("\n")[0];
        String queryString = requestString.split("\\?")[1];
        String params[] = queryString.split("&");
        Map<String, String> result = new HashMap<>();

        for (String param : params) {
            String splitting[] = param.split("=");
            result.put(splitting[0], splitting[1]);
        }

        return result;
    }

    public Map<String, String> getHeaders() {
        request = request.substring(request.indexOf('\n'));
        String splitParameters[] = request.split(": ");
        Map<String, String> headers = new HashMap<>();

        for (int i = 0; i < splitParameters.length - 1; i += 2) {
            headers.put(splitParameters[i], splitParameters[i + 1]);
        }

        return headers;
    }

    public String getPath() throws ServerException {
        int from = request.indexOf(" ") + 1;
        int to = request.indexOf(" ", from);

        String uri = request.substring(from, to);
        if (((uri.lastIndexOf('/')) == uri.length() - 1) && (uri.length() > 1)) {
            throw new ServerException(404);
        }

        return DEFAULT_FILES_DIR + uri;
    }

    public String getMethod() throws ServerException {
        String str[] = request.split(" ");

        if (!str[0].equals("GET")) {
            throw new ServerException(405);
        }

        return str[0];
    }
}

