package ru.nsu.fit.Seleznev.server;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) {
        int DEFAULT_PORT = 8080;

        Worker.instanceTree();

        try {
            ServerSocket serverSocket = new ServerSocket(DEFAULT_PORT, 100);
            while (true) {
                try (Socket clientSocket = serverSocket.accept()) {
                    try (InputStream in = clientSocket.getInputStream()) {
                        try (OutputStream out = clientSocket.getOutputStream()) {
                            Worker worker = new Worker(in, out);
                            worker.run();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
