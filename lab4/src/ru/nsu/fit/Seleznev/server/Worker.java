package ru.nsu.fit.Seleznev.server;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Handlers.*;
import ru.nsu.fit.Seleznev.server.Matchers.*;
import ru.nsu.fit.Seleznev.server.Tree.Tree;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Worker implements Runnable {
    private static Tree tree;
    private HttpResponse response;
    private InputStream in;
    private OutputStream out;

    Worker(InputStream in, OutputStream out) {
        this.in = in;
        this.out = out;
    }

    public static void instanceTree() {
        tree = new Tree();
        tree.register(Collections.singletonList(new StringMatcher("static", null)), new PathHandler());
        //tree.register(Arrays.asList(new StringMatcher("static", null), new StringMatcher("folder", null)), new PathHandler());
        //tree.register(Arrays.asList(new StringMatcher("static", null), new StringMatcher("newfolder", null)), new PathHandler());
    }

    @Override
    public void run() {
        try {
            response = new HttpResponse();
            HttpRequest request = new HttpRequest(in);
            HttpHandler handler = tree.getHandler(List.of(request.getPath().split("/")), request);
            response = handler.handle(request);


        } catch (ServerException e) {
            response.setCodeResponse(e.err);
        } finally {
            response.writeResponse(out);
        }
    }
}
