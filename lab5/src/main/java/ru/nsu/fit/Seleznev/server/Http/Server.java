package ru.nsu.fit.Seleznev.server.Http;

import ru.nsu.fit.Seleznev.server.Plugins.JarClassLoader;
import ru.nsu.fit.Seleznev.server.Plugins.ServerPlugin;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;
import ru.nsu.fit.Seleznev.server.ThreadPool.ThreadPool;
import ru.nsu.fit.Seleznev.server.Tree.Tree;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.jar.JarFile;


public class Server {
    private int DEFAULT_PORT = 8080;
    private Tree tree;
    private boolean isStopped = false;
    private ArrayList<ServerPlugin> serverPlugins;
    private ThreadPool threadPool;
    private String pathToPlugins;
    private Bootstrapper bootstrapper = null;
    private ServerSocket serverSocket;

    public Server() {
        serverPlugins = new ArrayList<>();
        threadPool = new ThreadPool(4);
        tree = new Tree();
        pathToPlugins = "Plugins.txt";
    }

    public void run(){
        try {
        serverSocket = new ServerSocket(DEFAULT_PORT, 100);
            while (!isStopped) {
                Socket clientSocket = serverSocket.accept();
                Connection connection = new Connection(tree, bootstrapper, clientSocket);
                threadPool.submit(connection);
            }
            serverSocket.close();
        }
        catch (IOException ignored) {
        }
    }

    public void initializePlugins() {
        String path;
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathToPlugins));
            while ((path = br.readLine()) != null) {
                loadPlugin(path);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Plugin Not Found");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (ServerPlugin plugin : serverPlugins) {
            plugin.initialize(bootstrapper);
        }
    }

    public void registerPlugin(String path) {
        try {
            JarFile plugin = new JarFile(path);
        }
        catch (Exception e) {
            return;
        }

        try {
            Files.write(Paths.get(pathToPlugins), (path + "\n").getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadPlugin(String path) {
        try {
            JarClassLoader classLoader = new JarClassLoader(new URL("file:" + path));
            String className = classLoader.getMainClassName();
            ServerPlugin plugin = (ServerPlugin) classLoader.invokeClass(className, null);
            serverPlugins.add(plugin);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Tree tree() {
        return tree;
    }

    public void stop() {
        isStopped = true;
        threadPool.pause();
    }

    public ArrayList<ServerPlugin> serverPlugins() {
        return serverPlugins;
    }

    public void setIsStopped(boolean isStopped) {
        this.isStopped = isStopped;
    }

    public void setBootstrapper(Bootstrapper bootstrapper) {
        this.bootstrapper = bootstrapper;
    }


    public ServerSocket serverSocket() {
        return serverSocket;
    }
}
