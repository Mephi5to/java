package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;

public class IntMatcher implements SegmentMatcher {
    private String name;

    public IntMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null || object.getClass() != this.getClass()) {
            return false;
        }
        IntMatcher matcher = (IntMatcher) object;

        return matcher.name.equals(this.name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((name == null) ? 0 : name.hashCode());

        return result;
    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }

        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }

    @Override
    public boolean isPartOfPath(String str) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    @Override
    public String getName() {
        return name;
    }
}
