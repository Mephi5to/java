package ru.nsu.fit.Seleznev.server.Http;

import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.io.*;
import java.util.ArrayList;

public class HttpResponse {
    public static final int INTERNAL_EXCEPTION = 500;
    private static final String VERSION = "HTTP/1.1";
    private static final int OK = 200;
    private OutputStream out;
    private Integer codeResponse;
    private ArrayList<String> headers;
    private HttpRequest request;
    private ByteArrayOutputStream body;

    public HttpResponse(OutputStream outputStream) {
        codeResponse = OK;
        headers = new ArrayList<>();
        out = outputStream;
        body = new ByteArrayOutputStream();
    }

    private void setHeaders(Integer length) {
        headers.add("Content-Type: text/html");
        headers.add("Content-Length: " + length.toString());
        headers.add("Connection: close");
    }

    public void addHeader(String header) {
        headers.add(header);
    }

    public void setMetricResult(Bootstrapper bootstrapper) {
        bootstrapper.metrics().addMetric(request.queryString(), System.currentTimeMillis() - request.StartTime());
    }

    private void setCodeResponse(Integer code) {
        codeResponse = code;
    }

    public void attachByteFile(ByteArrayOutputStream body) {
        this.body = body;
    }

    public void sendResponse() {
        try {
            if (codeResponse != 200) {
                out.write(makeResponse().getBytes());
                out.close();
                return;
            }
            setHeaders(body.size());

            out.write(makeResponse().getBytes());
            body.writeTo(out);
            out.close();
        } catch (IOException e) {
            sendErr(500);
        }
    }

    public void sendErr(int codeErr) {
        setCodeResponse(codeErr);
        sendResponse();
    }

    private String makeResponse() {
        String answer = VERSION + " " + getAnswer(codeResponse) + "\n";
        StringBuilder bldr = new StringBuilder(answer);

        if (codeResponse != OK) {
            bldr.append("Connection: close\n\n");
            return bldr.toString();
        }

        for (String header : headers) {
            bldr.append(header);
            bldr.append("\n");
        }
        bldr.append("\n");

        return bldr.toString();
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "200 OK";
            case 401:
                return "401 Unauthorized";
            case 404:
                return "404 Not Found";
            case 405:
                return "405 Method Not Allowed";
            default:
                return "500 Internal Server Error";
        }
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }
}
