package ru.nsu.fit.Seleznev.server.Database;

import java.util.HashMap;
import java.util.Map;

public class UsersInformation {
    private static final Map<String, String> users = createMap();

    private static Map<String, String> createMap()
    {
        HashMap<String,String> users = new HashMap<String,String>();

        users.put("admin", "qwerty");

        return users;
    }

    public boolean isCorrectLogin(String login, String password) {
        return login != null && users.get(login) != null && password != null && users.get(login).equals(password);
    }
}
