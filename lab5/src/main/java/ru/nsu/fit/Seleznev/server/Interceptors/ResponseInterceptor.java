package ru.nsu.fit.Seleznev.server.Interceptors;


import ru.nsu.fit.Seleznev.server.Http.HttpResponse;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

public interface ResponseInterceptor {
    void handleIntercept(HttpResponse response, Bootstrapper bootstrapper);
}
