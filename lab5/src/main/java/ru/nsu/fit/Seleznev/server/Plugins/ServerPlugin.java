package ru.nsu.fit.Seleznev.server.Plugins;

import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

public interface ServerPlugin {
    void initialize(Bootstrapper bootstrapper);
    String getName();

}
