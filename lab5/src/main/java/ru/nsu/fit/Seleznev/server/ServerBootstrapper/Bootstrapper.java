package ru.nsu.fit.Seleznev.server.ServerBootstrapper;

import ru.nsu.fit.Seleznev.server.Database.Metric;
import ru.nsu.fit.Seleznev.server.Http.Server;

import java.io.IOException;
import java.util.ArrayList;

public class Bootstrapper {
    private Server server;
    private ArrayList<Server> rebootList;
    private Metric metrics;

    public Bootstrapper(Server server, ArrayList<Server> rebootList) {
        this.server = server;
        this.rebootList = rebootList;
        metrics = new Metric();
    }

    public void start() {
        server.setIsStopped(false);
        rebootList.add(server);
    }

    public void restart() {
        server.stop();
        interruptListening();
        server = new Server();
        server.setBootstrapper(this);
        server.initializePlugins();
        rebootList.add(server);
        metrics = new Metric();
        synchronized (this) {
            this.notify();
        }
    }

    public void stop() {
        server.stop();
        rebootList.add(server);
    }

    public void registerPlugin(String path) {
        server.registerPlugin(path);
    }

    public Server server() {
        return server;
    }

    public Metric metrics() {
        return metrics;
    }

    private void interruptListening() {
        try {
            server.serverSocket().close();
        } catch (IOException ignored) {
        }
    }
}
