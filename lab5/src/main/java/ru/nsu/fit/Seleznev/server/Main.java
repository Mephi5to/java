package ru.nsu.fit.Seleznev.server;

import ru.nsu.fit.Seleznev.server.Http.Server;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.io.InterruptedIOException;
import java.net.SocketException;
import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        ArrayList<Server> rebootList = new ArrayList<>();
        Bootstrapper bootstrapper = new Bootstrapper(new Server(), rebootList);
        bootstrapper.server().setBootstrapper(bootstrapper);
        bootstrapper.server().initializePlugins();
        rebootList.add(bootstrapper.server());

        while (true) {
            if (!rebootList.isEmpty()) {
                rebootList.remove(rebootList.size() - 1).run();
            }
            synchronized (bootstrapper) {
                try {
                    bootstrapper.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

