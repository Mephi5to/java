package ru.nsu.fit.Seleznev.server.Interceptors;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

public class InputTimeInterceptor implements RequestInterceptor {
    @Override
    public void handleIntercept(HttpRequest request, Bootstrapper bootstrapper) {
        request.setStart();
    }
}
