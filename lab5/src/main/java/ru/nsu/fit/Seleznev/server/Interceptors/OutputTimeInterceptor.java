package ru.nsu.fit.Seleznev.server.Interceptors;

import ru.nsu.fit.Seleznev.server.Http.HttpResponse;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

public class OutputTimeInterceptor implements ResponseInterceptor {
    @Override
    public void handleIntercept(HttpResponse response, Bootstrapper bootstrapper) {
        response.setMetricResult(bootstrapper);
    }
}
