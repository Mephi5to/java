package ru.nsu.fit.Seleznev.server.Tree;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Handlers.AdminHandler;
import ru.nsu.fit.Seleznev.server.Handlers.HttpHandler;
import ru.nsu.fit.Seleznev.server.Handlers.PathHandler;
import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.Matchers.SegmentMatcher;
import ru.nsu.fit.Seleznev.server.Matchers.StringMatcher;
import ru.nsu.fit.Seleznev.server.Pipeline.IncomingPipeline;
import ru.nsu.fit.Seleznev.server.Pipeline.IncomingPipelineStage;
import ru.nsu.fit.Seleznev.server.Pipeline.OutcomingPipeline;
import ru.nsu.fit.Seleznev.server.Pipeline.OutcomingPipelineStage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Tree {
    private Node node;

    public Tree() {
    }

//    private void instanceTree() {
//        register(Collections.singletonList(new StringMatcher("static", null)), new PathHandler());
//        register(Arrays.asList(new StringMatcher("static", null), new StringMatcher("users", null),
//                new StringMatcher("admin", "login")), new AdminHandler());
//        register(Arrays.asList(new StringMatcher("static", null), new StringMatcher("folder", null)), new PathHandler());
//        //tree.register(Arrays.asList(new StringMatcher("static", null), new StringMatcher("newfolder", null)), new PathHandler());
//    }

    public void registerIncomingStage(IncomingPipelineStage stage, String path) {
        String[] pathElements = path.split("/");
        Node cur = node;

        for (String str : pathElements) {
            if (cur.matcher.isPartOfPath(str)) {
                continue;
            }
            for (Node node1 : cur.nodes) {
                if (node1.matcher.isPartOfPath(str)) {
                    cur = node1;
                    break;
                }
            }
        }

        cur.addIncomingStage(stage);
    }

    public void registerOutcomingStage(OutcomingPipelineStage stage, String path) {
        String[] pathElements = path.split("/");
        Node cur = node;

        for (String str : pathElements) {
            if (cur.matcher.isPartOfPath(str)) {
                continue;
            }
            for (Node node1 : cur.nodes) {
                if (node1.matcher.isPartOfPath(str)) {
                    cur = node1;
                    break;
                }
            }
        }

        cur.addOutcomingStage(stage);
    }

    private void fillPipelines(Node cur, IncomingPipeline incomingPipeline, OutcomingPipeline outcomingPipeline) {
        for (IncomingPipelineStage stage : cur.incomingPipelineStages) {
            stage.addToPipeLine(incomingPipeline);
        }
        for (OutcomingPipelineStage stage : cur.outcomingPipelineStages) {
            stage.addToPipeLine(outcomingPipeline);
        }
    }

    public HttpHandler getHandler(List<String> segments, HttpRequest request, IncomingPipeline incomingPipeline, OutcomingPipeline outcomingPipeline) throws ServerException {
        Node cur = node;
        for (String segment : segments) {
            if (!cur.matcher.isMatching(segment, request)) {
                for (Node iter : cur.nodes) {
                    if (iter.matcher.isMatching(segment, request)) {
                        cur = iter;
                        fillPipelines(cur, incomingPipeline, outcomingPipeline);
                    }
                }
            } else {
                fillPipelines(cur, incomingPipeline, outcomingPipeline);
            }
        }

        if (cur.handler == null) {
            throw new ServerException(404);
        }

        return cur.handler;

    }

    public void register(List<SegmentMatcher> matchers, HttpHandler handler) {
        Node cur = node;
        boolean flag;
        for (SegmentMatcher matcher : matchers) {
            flag = false;
            if (cur == null) {
                cur = new Node(matcher);
                node = cur;
            } else {
                if (cur.matcher.equals(matcher)) {
                    continue;
                }
                if (cur.nodes.isEmpty()) {
                    Node node = new Node(matcher);
                    cur.nodes.add(node);
                    cur = node;
                } else {
                    for (Node iter : cur.nodes) {
                        if (iter.matcher.equals(matcher)) {
                            cur = iter;
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        Node node = new Node(matcher);
                        cur.nodes.add(node);
                        cur = node;
                    }
                }
            }
        }
        cur.handler = handler;
    }

    private class Node {
        private ArrayList<Node> nodes;
        private SegmentMatcher matcher;
        private HttpHandler handler;
        private ArrayList<IncomingPipelineStage> incomingPipelineStages;
        private ArrayList<OutcomingPipelineStage> outcomingPipelineStages;

        Node(SegmentMatcher matcher) {
            incomingPipelineStages = new ArrayList<>();
            outcomingPipelineStages = new ArrayList<>();
            this.matcher = matcher;
            nodes = new ArrayList<>();
            handler = null;
        }

        private void addIncomingStage(IncomingPipelineStage stage) {
            incomingPipelineStages.add(stage);
        }

        private void addOutcomingStage(OutcomingPipelineStage stage) {
            outcomingPipelineStages.add(stage);
        }

    }
}
