package ru.nsu.fit.Seleznev.server.Pipeline;

import ru.nsu.fit.Seleznev.server.Interceptors.RequestInterceptor;

public class IncomingPipelineStage {
    private String name;
    private RequestInterceptor interceptor;
    private String location;
    private String nameNeighbour = null;

    public IncomingPipelineStage(String name, RequestInterceptor interceptor, String location) {
        this.name = name;
        this.interceptor = interceptor;
        this.location = location;
    }

    public IncomingPipelineStage(String name, RequestInterceptor interceptor, String location, String nameNeighbour) {
        this.name = name;
        this.interceptor = interceptor;
        this.location = location;
        this.nameNeighbour = nameNeighbour;
    }

    public String getName() {
        return name;
    }

    public RequestInterceptor getInterceptor() {
        return interceptor;
    }

    public void addToPipeLine(IncomingPipeline pipeline) {
        switch (location) {
            case "AddAfter":
                pipeline.addAfter(nameNeighbour, this);
                break;
            case "AddBefore":
                pipeline.addBefore(nameNeighbour, this);
                break;
            case "AddFirst":
                pipeline.addFirst(this);
                break;
            default:
                pipeline.addLast(this);
        }
    }
}
