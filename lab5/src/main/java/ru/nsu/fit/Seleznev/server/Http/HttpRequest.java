package ru.nsu.fit.Seleznev.server.Http;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class HttpRequest {
    private static final String DEFAULT_FILES_DIR = "static";
    private String request;
    private Map<String, String> pathInfo;
    private Long startTime;
    private InputStream body = null;

    HttpRequest(InputStream inputStream) throws ServerException {
        pathInfo = new HashMap<>();
        initialize(inputStream);
    }

    private void initialize(InputStream inputStream) throws ServerException {
        boolean flag = false;
        byte[] array = new byte[1024];
        int err;
        try {
            for (int i = 0; !flag; ++i) {
                err = inputStream.read(array, i, 1);
                if (err < 0) {
                    throw new ServerException(500);
                }
                if (i < 2) {
                    continue;
                }
                if ((array[i] == '\n') && (array[i - 1] == '\n') || ((array[i - 1] == '\r') && (array[i - 2] == '\n'))) {
                    flag = true;
                }
            }
        } catch (IOException e) {
            throw new ServerException(HttpResponse.INTERNAL_EXCEPTION);
        }

        body = inputStream;
        request = new String(array);
    }

    public void addInfo(String key, String value) {
        pathInfo.put(key, value);
    }

    public String info(String key) {
        return pathInfo.get(key);
    }

    public String queryString() {
        return request.split("\n")[0];
    }

    public Map<String, String> parameters() {
        String requestString = request.split("\n")[0];
        String queryString;
        String params[] = new String[]{};
        Map<String, String> result = new HashMap<>();

        if (!requestString.contains("?")) {
            return result;
        }

        queryString = requestString.split("\\?")[1];
        params = queryString.split("&");
        params[params.length - 1] = params[params.length - 1].split(" ")[0];
        String[] splitting;
        for (String param : params) {
            splitting = param.split("=");
            String value = splitting.length < 2 ? null : splitting[1];
            result.put(splitting[0], value);
        }

        return result;
    }

    public Map<String, String> headers() {
        String[] strings = request.split("\n");
        Map<String, String> headers = new HashMap<>();
        String[] splitting;
        for (int i = 1; i < strings.length - 2; ++i) {
            splitting = strings[i].split(": ");
            String value = splitting.length < 2 ? null : splitting[1];
            headers.put(splitting[0], value);
        }

        return headers;
    }

    public String path() throws ServerException {
        int from = request.indexOf(" ") + 1;
        int to = request.indexOf(" ", from);

        String uri = request.substring(from, to);
        if (uri.contains("?")) {
            uri = uri.split("\\?")[0];
        }
        if (((uri.lastIndexOf('/')) == uri.length() - 1) && (uri.length() > 1)) {
            throw new ServerException(404);
        }

        return DEFAULT_FILES_DIR + uri;
    }

    public String Method() throws ServerException {
        String str[] = request.split(" ");

        if (!str[0].equals("GET")) {
            throw new ServerException(405);
        }

        return str[0];
    }

    public long StartTime() {
        return startTime;
    }

    public void setStart() {
        startTime = System.currentTimeMillis();
    }
}

