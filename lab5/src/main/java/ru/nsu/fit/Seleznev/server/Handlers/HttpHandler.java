package ru.nsu.fit.Seleznev.server.Handlers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.Http.HttpResponse;
import ru.nsu.fit.Seleznev.server.Pipeline.OutcomingPipeline;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.io.OutputStream;

public abstract class HttpHandler {
    HttpRequest request = null;
    HttpResponse response = null;
    Bootstrapper bootstrapper = null;
    OutcomingPipeline outcomingPipeline = null;

    public abstract void run();

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public void setBootstrapper(Bootstrapper bootstrapper) {
        this.bootstrapper = bootstrapper;
    }

    public void setOutcomingPipeline(OutcomingPipeline outcomingPipeline) {
        this.outcomingPipeline = outcomingPipeline;
    }

    public void setResponse(OutputStream out) {
        response = new HttpResponse(out);
        response.setRequest(request);
    }
}
