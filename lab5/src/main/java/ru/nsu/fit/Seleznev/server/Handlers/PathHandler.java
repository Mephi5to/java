package ru.nsu.fit.Seleznev.server.Handlers;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PathHandler extends HttpHandler {
    @Override
    public void run() {
        try {
            String path = request.path();
            path = path.replace('/', File.separatorChar);
            File file = Paths.get(path).toFile();

            if (file.isDirectory()) {
                path += File.separatorChar + "index.html";
                file = Paths.get(path).toFile();
            }

            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            Files.copy(file.toPath(), byteArray);
            response.attachByteFile(byteArray);
            outcomingPipeline.doWork(response, bootstrapper);
            response.sendResponse();

        } catch (ServerException e) {
            response.sendErr(e.err);
        }
        catch (IOException e) {
            response.sendErr(500);
        }
    }
}
