package ru.nsu.fit.Seleznev.server.Pipeline;

import ru.nsu.fit.Seleznev.server.Http.HttpResponse;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.util.ArrayList;

public class OutcomingPipeline {
    private ArrayList<OutcomingPipelineStage> stages;

    public OutcomingPipeline() {
        stages = new ArrayList<>();
    }

    public OutcomingPipeline(ArrayList<OutcomingPipelineStage> stages) {
        this.stages = stages;
    }

    public void doWork(HttpResponse response, Bootstrapper bootstrapper) {
        for (OutcomingPipelineStage stage : stages) {
            stage.getInterceptor().handleIntercept(response, bootstrapper);
        }
    }

    public boolean isEmpty() {
        return stages.isEmpty();
    }

    public void addLast(OutcomingPipelineStage stage) {
        stages.add(stage);
    }

    public void addFirst(OutcomingPipelineStage stage) {
        stages.add(0, stage);
    }

    public void addAfter(String name, OutcomingPipelineStage stage) {
        int index = 0;

        for (OutcomingPipelineStage iter : stages) {
            index++;
            if (iter.getName().equals(name)) {
                break;
            }
        }

        stages.add(index, stage);
    }

    public void addBefore(String name, OutcomingPipelineStage stage) {
        int index = 0;

        for (OutcomingPipelineStage iter : stages) {
            if (iter.getName().equals(name)) {
                break;
            }
            index++;
        }

        stages.add(index, stage);
    }
}
