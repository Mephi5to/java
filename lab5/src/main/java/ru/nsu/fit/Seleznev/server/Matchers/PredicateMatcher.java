package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;

import java.util.function.Predicate;

public class PredicateMatcher implements SegmentMatcher {
    private Predicate predicate;
    private String name;

    PredicateMatcher(Predicate<String> predicate, String name) {
        this.predicate = predicate;
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((predicate == null) ? 0 : predicate.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null || object.getClass() != this.getClass()) {
            return false;
        }
        PredicateMatcher matcher = (PredicateMatcher) object;

        return matcher.predicate.equals(this.predicate);
    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        if (!predicate.toString().equals(str)) {
            return false;
        }
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }

    @Override
    public boolean isPartOfPath(String str) {
        return predicate.toString().equals(str);
    }

    @Override
    public String getName() {
        return name;
    }
}
