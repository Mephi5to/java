package ru.nsu.fit.Seleznev.server.Pipeline;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.util.ArrayList;

public class IncomingPipeline {
    private ArrayList<IncomingPipelineStage> stages;

    public IncomingPipeline() {
        stages = new ArrayList<>();
    }

    public IncomingPipeline(ArrayList<IncomingPipelineStage> stages) {
        this.stages = stages;
    }

    public void doWork(HttpRequest request, Bootstrapper bootstrapper) throws ServerException {
        for (IncomingPipelineStage stage : stages) {
            stage.getInterceptor().handleIntercept(request, bootstrapper);
        }
    }

    public boolean isEmpty() {
        return stages.isEmpty();
    }

    public void addLast(IncomingPipelineStage stage) {
        stages.add(stage);
    }

    public void addFirst(IncomingPipelineStage stage) {
        stages.add(0, stage);
    }

    public void addAfter(String name, IncomingPipelineStage stage) {
        int index = stages.size() - 1;

        for (IncomingPipelineStage iter : stages) {
            index++;
            if (iter.getName().equals(name)) {
                break;
            }
        }

        stages.add(index, stage);
    }

    public void addBefore(String name, IncomingPipelineStage stage) {
        int index = 0;

        for (IncomingPipelineStage iter : stages) {
            if (iter.getName().equals(name)) {
                break;
            }
            index++;
        }

        stages.add(index, stage);
    }

}
