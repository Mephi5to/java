package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;

public class StringMatcher implements SegmentMatcher {
    private String segment;
    private String name;

    public StringMatcher(String segment, String name) {
        this.segment = segment;
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((segment == null) ? 0 : segment.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null || object.getClass() != this.getClass()) {
            return false;
        }
        StringMatcher matcher = (StringMatcher) object;

        return matcher.hashCode() == this.hashCode();
    }



    public boolean isMatching(String str, HttpRequest request) {
        if (!segment.equals(str)) {
            return false;
        }
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }

    @Override
    public boolean isPartOfPath(String str) {
        return segment.equals(str);
    }

    @Override
    public String getName() {
        return name;
    }
}
