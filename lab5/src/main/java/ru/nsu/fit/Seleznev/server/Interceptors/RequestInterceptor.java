package ru.nsu.fit.Seleznev.server.Interceptors;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

public interface RequestInterceptor {
    void handleIntercept(HttpRequest request, Bootstrapper bootstrapper) throws ServerException;
}
