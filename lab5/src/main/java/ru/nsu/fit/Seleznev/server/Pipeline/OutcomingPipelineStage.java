package ru.nsu.fit.Seleznev.server.Pipeline;

import ru.nsu.fit.Seleznev.server.Interceptors.ResponseInterceptor;

public class OutcomingPipelineStage {
    private String name;
    private ResponseInterceptor interceptor;
    private String  location;
    private String nameNeighbour = null;

    OutcomingPipelineStage(String name, ResponseInterceptor interceptor, String location, String nameNeighbour) {
        this.name = name;
        this.interceptor = interceptor;
        this.location = location;
        this.nameNeighbour = nameNeighbour;
    }

    public OutcomingPipelineStage(String name, ResponseInterceptor interceptor, String location) {
        this.name = name;
        this.interceptor = interceptor;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public ResponseInterceptor getInterceptor() {
        return interceptor;
    }

    public void addToPipeLine(OutcomingPipeline pipeline) {
        switch (location) {
            case "AddBefore":
                pipeline.addBefore(nameNeighbour, this);
                break;
            case "AddAfter":
                pipeline.addAfter(nameNeighbour, this);
                break;
            case "AddFirst":
                pipeline.addFirst(this);
                break;
            default:
                pipeline.addLast(this);
        }
    }
}
