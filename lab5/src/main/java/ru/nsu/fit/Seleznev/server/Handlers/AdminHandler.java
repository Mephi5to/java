package ru.nsu.fit.Seleznev.server.Handlers;

import ru.nsu.fit.Seleznev.server.Plugins.ServerPlugin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class AdminHandler extends  HttpHandler {
    Map<String, String> headers;

    @Override
    public void run() {
        headers = request.headers();
        String command = headers.get("Command");

        spotFunction(command);
        response.setMetricResult(bootstrapper);
    }

    private void spotFunction(String command) {
        switch (command) {
            case "reboot":
                bootstrapper.restart();
                break;
            case "pluginList":
                showPluginList(bootstrapper.server().serverPlugins());
                break;
            case "metric":
                showMetrics();
                break;
            case "registerPlugin":
                registerPlugin(headers.get("Path"));
                break;
        }
    }

    private void registerPlugin(String path) {
        bootstrapper.registerPlugin(path);
    }

    private void showMetrics() {
        ByteArrayOutputStream body = new ByteArrayOutputStream();

        try {
            body.write(bootstrapper.metrics().getListMetrics().getBytes());
            response.attachByteFile(body);
            response.sendResponse();
        }
        catch (IOException e) {
            response.sendErr(500);
        }
    }

    private void showPluginList(ArrayList<ServerPlugin> plugins) {
        ByteArrayOutputStream body = new ByteArrayOutputStream();

        try {
            for (ServerPlugin plugin : plugins) {
                body.write((plugin.getName()+"\n").getBytes());
            }
            if (plugins.isEmpty()) {
                body.write("No working plugins at the moment".getBytes());
            }
            response.attachByteFile(body);
            response.sendResponse();
        }
        catch (IOException e) {
            response.sendErr(500);
        }
    }
}
