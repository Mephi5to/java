package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;

public class AnyMatcher implements SegmentMatcher {
    private String name;

    AnyMatcher(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((name == null) ? 0 : name.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object object) {
        return object.getClass() == this.getClass();
    }

    @Override
    public boolean isMatching(String str, HttpRequest request) {
        if (name != null) {
            request.addInfo(name, str);
        }

        return true;
    }

    @Override
    public boolean isPartOfPath(String str) {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }
}
