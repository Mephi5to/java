package ru.nsu.fit.Seleznev.server.ThreadPool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ThreadPool {
    private LinkedList<Runnable> taskQueue;
    private List<WorkerThread> threads;
    private boolean isStopped = false;

    public ThreadPool(int count) {
        threads = new ArrayList<>(count);
        taskQueue = new LinkedList<>();

        for (int i = 0; i < count; ++i) {
            threads.add(new WorkerThread(taskQueue));
        }
        for (WorkerThread thread : threads) {
            thread.start();
        }
    }

    synchronized public void submit(Runnable task) {
        synchronized (taskQueue) {
            if (!isStopped) {
                taskQueue.addLast(task);
                taskQueue.notify();
            }
        }
    }

    synchronized public void pause() {
        isStopped = true;
        while (!taskQueue.isEmpty()) {
            taskQueue.clear();
        }
        for (WorkerThread thread : threads) {
            thread.stopThread();
            if (thread != Thread.currentThread()) {
                thread.interrupt();
            }
        }
    }

    public boolean isStopped() {
        return isStopped;
    }

    synchronized public void start() {
        if (isStopped) {
            isStopped = false;
        }
    }

    synchronized public void stop() {
        for (WorkerThread thread : threads) {
            thread.stopThread();
        }
    }
}
