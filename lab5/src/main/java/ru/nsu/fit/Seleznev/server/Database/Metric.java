package ru.nsu.fit.Seleznev.server.Database;

import java.util.ArrayList;

public class Metric {
    private ArrayList<String> metrics;

    public Metric() {
        metrics = new ArrayList<>();
    }

    public void addMetric(String url, Long time) {
        metrics.add(url + " " + time + "mls");
    }

    public String getListMetrics() {
        StringBuilder bldr = new StringBuilder();

        for (String str : metrics) {
            bldr.append(str).append("\n");
        }

        return bldr.toString();
    }
}
