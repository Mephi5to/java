package ru.nsu.fit.Seleznev.server.Interceptors;

import ru.nsu.fit.Seleznev.server.Database.UsersInformation;
import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Http.HttpRequest;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;

import java.util.Map;

public class AuthorizingInterceptor implements RequestInterceptor {
    @Override
    public void handleIntercept(HttpRequest request, Bootstrapper bootstrapper) throws ServerException {
        String authorized = request.headers().get("Cookie");
        UsersInformation info = new UsersInformation();
        Map<String, String> params = request.parameters();

        if (!info.isCorrectLogin(request.info("login"), params.get("password")) && !"isAuthorized".equals(authorized)) {
            throw new ServerException(401);
        }

        if ("isAuthorized".equals(authorized)) {
            return;
        }

        throw new ServerException(200);
    }
}
