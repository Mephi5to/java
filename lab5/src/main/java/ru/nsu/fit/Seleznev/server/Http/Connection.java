package ru.nsu.fit.Seleznev.server.Http;

import ru.nsu.fit.Seleznev.server.Exceptions.ServerException;
import ru.nsu.fit.Seleznev.server.Handlers.HttpHandler;
import ru.nsu.fit.Seleznev.server.Pipeline.IncomingPipeline;
import ru.nsu.fit.Seleznev.server.Pipeline.OutcomingPipeline;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;
import ru.nsu.fit.Seleznev.server.Tree.Tree;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

public class Connection implements Runnable {
    private Tree tree;
    private HttpResponse response;
    private Socket clientSocket;
    private Bootstrapper bootstrapper;

    Connection(Tree tree, Bootstrapper bootstrapper, Socket clientSocket) {
        this.tree = tree;
        this.bootstrapper = bootstrapper;
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try (InputStream in = clientSocket.getInputStream()) {
            try (OutputStream out = clientSocket.getOutputStream()) {
                try {
                    IncomingPipeline incomingPipeline = new IncomingPipeline();
                    OutcomingPipeline outcomingPipeline = new OutcomingPipeline();
                    HttpRequest request = new HttpRequest(in);
                    HttpHandler handler = tree.getHandler(List.of(request.path().split("/")), request, incomingPipeline, outcomingPipeline);
                    incomingPipeline.doWork(request, bootstrapper);
                    setHandlerAttributes(handler, request, out, outcomingPipeline);
                    handler.run();
                }
                catch (ServerException e) {
                    response = new HttpResponse(out);
                    response.sendErr(e.err);
                }
            }
        }
        catch (IOException ignored) {
        }

        closeSocket();
    }

    private void closeSocket() {
        try {
            clientSocket.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setHandlerAttributes(HttpHandler handler, HttpRequest request, OutputStream out, OutcomingPipeline outcomingPipeline) {
        handler.setRequest(request);
        handler.setResponse(out);
        handler.setOutcomingPipeline(outcomingPipeline);
        handler.setBootstrapper(bootstrapper);
    }
}
