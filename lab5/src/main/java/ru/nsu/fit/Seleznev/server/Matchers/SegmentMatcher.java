package ru.nsu.fit.Seleznev.server.Matchers;

import ru.nsu.fit.Seleznev.server.Http.HttpRequest;

public interface SegmentMatcher {
    @Override
            boolean equals(Object object);
    @Override
            int hashCode();

    boolean isMatching(String str, HttpRequest request);

    boolean isPartOfPath(String str);

    String getName();
}
