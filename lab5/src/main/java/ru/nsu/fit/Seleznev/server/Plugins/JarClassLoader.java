package ru.nsu.fit.Seleznev.server.Plugins;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Attributes;

public class JarClassLoader extends URLClassLoader {
    private URL url;

    public JarClassLoader(URL url) {
        super(new URL[] { url });
        this.url = url;
    }

    public String getMainClassName() throws IOException {
        URL u = new URL("jar", "", url + "!/");
        JarURLConnection uc = (JarURLConnection)u.openConnection();
        Attributes attr = uc.getMainAttributes();

        return attr != null ? attr.getValue(Attributes.Name.MAIN_CLASS) : null;
    }

    public Object invokeClass(String name, String[] args) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, InstantiationException {
        Class c = loadClass(name);

        return c.getConstructors()[0].newInstance();
    }
}
