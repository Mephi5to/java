package ru.nsu.fit.Seleznev.ThreadPool;

public class Main {
    private static int count = 0;
    public static void main(String[] args) {
	    Task task = new Task();
	    ThreadPool tp = new ThreadPool(10);

	    for (int i = 0; i < 1000; ++i) {
            tp.submit(task);
        }
    }

    public static class Task implements Runnable {

        public void run() {
            for (int i = 0; i < 1000000; ++i) {
                count++;
            }
        }
    }
}
