package ru.nsu.fit.Seleznev.ThreadPool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ThreadPool {
    private LinkedList<Runnable> taskQueue;
    private List<WorkerThread> threads;
    private boolean isStopped = false;

    ThreadPool(int count) {
        threads = new ArrayList<>(count);
        taskQueue = new LinkedList<>();

        for (int i = 0; i < count; ++i) {
            threads.add(new WorkerThread(taskQueue));
        }
        for (WorkerThread thread : threads) {
            thread.start();
        }
    }

    synchronized public void submit(Runnable task) {
            taskQueue.addLast(task);
            notify();
    }

    synchronized public void stop() {
        isStopped = true;
        for (WorkerThread thread : threads) {
            thread.stopThread();
        }
    }
}
