package ru.nsu.fit.Seleznev.ThreadPool;

import java.util.LinkedList;

public class WorkerThread extends Thread {
    private boolean isStopped = false;
    private LinkedList<Runnable> taskQueue;

    WorkerThread(LinkedList<Runnable> queue) {
        taskQueue = queue;
    }

    public void run() {
        while (!isStopped()) {
            synchronized (taskQueue) {
                while (taskQueue.isEmpty()) {
                    try {
                        taskQueue.wait();
                    } catch (InterruptedException ignored) {

                    }
                }
                Runnable runnable = taskQueue.removeFirst();
                runnable.run();
            }
        }

    }

    public synchronized void stopThread() {
        isStopped = true;
        this.interrupt();
    }

    public synchronized boolean isStopped() {
        return isStopped;
    }
}
