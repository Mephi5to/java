package ru.nsu.fit.Seleznev.server;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) {
        int DEFAULT_PORT = 8080;




        try {
            ServerSocket serverSocket = new ServerSocket(DEFAULT_PORT, 100);
            while (true) {
                try (Socket clientSocket = serverSocket.accept()) {
                    try (InputStream in = clientSocket.getInputStream()) {
                        try (OutputStream out = clientSocket.getOutputStream()) {
                            ResponseHandler responseHandler = new ResponseHandler(in, out);
                            responseHandler.writeResponse();

                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public class Move implements Runnable {
        public void run() {
        }
    }
}