package ru.nsu.fit.Seleznev.server;

import java.io.*;
import java.nio.file.Paths;

public class ResponseHandler {
    private static final String VERSION = "HTTP/1.1";
    private static final Integer OK = 200;
    private static final Integer INTERNAL_EXCEPTION = 500;
    private BufferedInputStream in;
    private DataOutputStream out;
    private String[] headers;

    ResponseHandler(InputStream in, OutputStream out) {
        this.in = new BufferedInputStream(in);
        this.out = new DataOutputStream(out);
    }

    private void setHeaders(Integer length) {
        headers = new String[3];
        headers[0] = "Content-Type: text/html";
        headers[1] = "Content-Length: " + length.toString();
        headers[2] = "Connection: close";
    }

    public void writeResponse() throws IOException {
        try {
            RequestHandler requestHandler = new RequestHandler(in);
            File file = handlePath(requestHandler.getPath());
            FileInputStream documentStream = new FileInputStream(file);
            setHeaders(documentStream.available());
            String response = makeResponse(OK);
            out.write(response.getBytes());
            int count;
            byte[] buffer = new byte[1024];
            while ((count = documentStream.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            documentStream.close();
            closeOutput();

        } catch (IOException e) {
            out.write(makeResponse(INTERNAL_EXCEPTION).getBytes());
            closeOutput();
        } catch (ServerException e) {
            out.write(makeResponse(e.err).getBytes());
            closeOutput();
        }
    }

    private File handlePath(String path) throws ServerException {
        try {
            path = path.replace('/', File.separatorChar);
            File file = Paths.get(path).toFile();

            if (file.isDirectory()) {
                path += File.separatorChar + "index.html";
                file = Paths.get(path).toFile();
            }

            return file;

        } catch (UnsupportedOperationException e) {
            throw new ServerException(404);
        }
    }

    private void closeOutput() throws IOException {
        out.flush();
        out.close();
    }

    private String makeResponse(int code) {
        String answer = VERSION + " " + getAnswer(code) + "\n";
        StringBuilder bldr = new StringBuilder(answer);

        if (code != OK) {
            bldr.append("Connection: close\n");
            return bldr.toString();
        }

        for (String header : headers) {
            bldr.append(header);
            bldr.append("\n");
        }
        bldr.append("\n");

        return bldr.toString();
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "200 OK";
            case 404:
                return "404 Not Found";
            case 405:
                return "405 Method Not Allowed";
            default:
                return "500 Internal Server Error";
        }
    }


}
