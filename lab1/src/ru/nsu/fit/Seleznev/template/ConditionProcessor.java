package ru.nsu.fit.Seleznev.template;


import java.util.Map;

class ConditionProcessor implements TemplateProcessor {
    private String string;
    private String condition;

    ConditionProcessor(String str, String condition) {
        string = str;
        this.condition = condition;
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (conditions.get(condition) == null) {
            throw new ArgumentNotFoundException();
        }
        if (conditions.get(condition)) {
            sb.append(string);
        }
    }
}