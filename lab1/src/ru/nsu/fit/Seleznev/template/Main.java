package ru.nsu.fit.Seleznev.template;


import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedWriter writer = new BufferedWriter(new FileWriter("result.txt"));

        String PATH = new File("template.txt").getAbsolutePath();
        BufferedReader br = new BufferedReader(new FileReader(PATH));
        String string;

        while ((string = br.readLine()) != null) {
            builder.append(string);
            builder.append("\n");
        }
        builder.deleteCharAt(builder.length() - 1);

        Map<String, Integer> iterations = new HashMap<>();
        Map<String, String> values = new HashMap<>();
        Map<String, Boolean> conditions = new HashMap<>();

        try {
            Template templ = new Template(builder.toString());
            writer.write(templ.fill(values, conditions, iterations));
        } catch (BadConstruction bad) {
            System.out.println("Bad Construction");
            System.exit(1);
        } catch (ArgumentNotFoundException except) {
            System.out.println("ArgumentNotFoundException");
            System.exit(1);
        }

        br.close();
        writer.close();
    }
}
