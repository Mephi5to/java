package ru.nsu.fit.Seleznev.template;

import java.util.Map;


interface TemplateProcessor {
    void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException;
}