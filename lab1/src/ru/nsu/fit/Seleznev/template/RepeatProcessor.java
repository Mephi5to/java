package ru.nsu.fit.Seleznev.template;


import java.util.Map;

class RepeatProcessor implements TemplateProcessor {
    private String string;
    private String count;

    RepeatProcessor(String str, String count) {
        string = str;
        this.count = count;
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (iterations.get(count) == null) {
            throw new ArgumentNotFoundException();
        }

        for (int i = 0; i < iterations.get(count); ++i) {
            sb.append(string);
        }

    }
}
