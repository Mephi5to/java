package ru.nsu.fit.Seleznev.template;

import java.util.Map;


class ValueProcessor implements TemplateProcessor {
    private String value;

    ValueProcessor(String value) {
        this.value = value;
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (values.get(value) != null) {
            sb.append(values.get(value));
        } else {
            throw new ArgumentNotFoundException();
        }
    }
}
