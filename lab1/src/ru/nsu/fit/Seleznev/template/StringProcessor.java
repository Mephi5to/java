package ru.nsu.fit.Seleznev.template;


import java.util.Map;

class StringProcessor implements TemplateProcessor {
    private String string;

    StringProcessor(String str) {
        string = str;
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        sb.append(string);
    }
}