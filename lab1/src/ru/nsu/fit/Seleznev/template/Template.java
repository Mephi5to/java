package ru.nsu.fit.Seleznev.template;


import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class Template {
    private List<TemplateProcessor> processors;

    Template(String str) throws BadConstruction {
        parse(str);
    }

    String fill(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        StringBuilder stringBuilder = new StringBuilder();

        for (TemplateProcessor processor : processors) {
            processor.fillTemplate(stringBuilder, values, conditions, iterations);
        }

        return stringBuilder.toString();
    }

    private void parse(String str) throws BadConstruction {
        String[] words = str.split("%", -1);
        StringBuilder bldr = new StringBuilder();
        processors = new LinkedList<>();
        boolean isForStringProcessor = true;

        for (int i = 0; i < words.length; ++i) {
            i = collectStrings(bldr, words, i);

            if (isForStringProcessor) {
                processors.add(new StringProcessor(bldr.toString()));
                bldr.delete(0, bldr.length());
                isForStringProcessor = false;
                continue;
            }

            if ((words[i].isEmpty()) || (words[i].charAt(0) != '!')) {
                processors.add(new ValueProcessor(words[i]));
                bldr.delete(0, bldr.length());
                isForStringProcessor = true;
                continue;
            }

            if (words[i].contains("!if")) {
                String[] conditionalCase = words[i].split(" ", 2);

                if (((i + 2) >= words.length) || (conditionalCase.length < 2) || (!conditionalCase[0].equals("!if"))) {
                    throw new BadConstruction();
                }

                bldr.delete(0, bldr.length());
                i++;
                i = collectStrings(bldr, words, i);

                processors.add(new ConditionProcessor(bldr.toString(), conditionalCase[1].substring(0, conditionalCase[1].length() - 1)));
                bldr.delete(0, bldr.length());
                i++;

                if ((i >= words.length) || !words[i].equals("!endif!")) {
                    throw new BadConstruction();
                }

                isForStringProcessor = true;
                continue;
            }

            if (words[i].contains("!repeat")) {
                String[] repeatCase = words[i].split(" ", 2);

                if (((i + 2) >= words.length) || (repeatCase.length < 2) || (!repeatCase[0].equals("!if"))) {
                    throw new BadConstruction();
                }

                bldr.delete(0, bldr.length());
                i++;
                i = collectStrings(bldr, words, i);

                processors.add(new RepeatProcessor(bldr.toString(), repeatCase[1].substring(0, repeatCase[1].length() - 1)));
                bldr.delete(0, bldr.length());
                i++;

                if ((i >= words.length) || !words[i].equals("!endrepeat!")) {
                    throw new BadConstruction();
                }

                isForStringProcessor = true;
            }

        }
    }

    private int collectStrings(StringBuilder bldr, String[] arr, int index) {
        bldr.append(arr[index]);

        while ((index < arr.length - 1) && (bldr.length() != 0) && (bldr.charAt(bldr.length() - 1) == '\\')) {
            bldr.deleteCharAt(bldr.length() - 1);
            bldr.append('%');
            index++;
            bldr.append(arr[index]);
        }

        return index;
    }
}