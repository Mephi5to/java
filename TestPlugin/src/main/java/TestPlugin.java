import ru.nsu.fit.Seleznev.server.Handlers.AdminHandler;
import ru.nsu.fit.Seleznev.server.Handlers.PathHandler;
import ru.nsu.fit.Seleznev.server.Interceptors.*;
import ru.nsu.fit.Seleznev.server.Matchers.*;
import ru.nsu.fit.Seleznev.server.Pipeline.IncomingPipelineStage;
import ru.nsu.fit.Seleznev.server.Pipeline.OutcomingPipelineStage;
import ru.nsu.fit.Seleznev.server.Plugins.ServerPlugin;
import ru.nsu.fit.Seleznev.server.ServerBootstrapper.Bootstrapper;
import ru.nsu.fit.Seleznev.server.Tree.Tree;


import java.util.Arrays;
import java.util.Collections;

public class TestPlugin implements ServerPlugin {
    private String name = "plugin1";

    public void initialize(Bootstrapper serverBootstrapper) {
        Tree tree = serverBootstrapper.server().tree();
        tree.register(Collections.<SegmentMatcher>singletonList(new StringMatcher("static", null)), new PathHandler());
        tree.register(Arrays.<SegmentMatcher>asList(new StringMatcher("static", null), new StringMatcher("users", null),
                new StringMatcher("admin", "login")), new AdminHandler());
        tree.register(Arrays.<SegmentMatcher>asList(new StringMatcher("static", null), new StringMatcher("folder", null)), new PathHandler());
        RequestInterceptor authorize = new AuthorizingInterceptor();

        IncomingPipelineStage authorization = new IncomingPipelineStage("authorization", authorize, "AddFirst");
        tree.registerIncomingStage(authorization, "static/users");

        RequestInterceptor inputTime = new InputTimeInterceptor();
        IncomingPipelineStage startTimePoint = new IncomingPipelineStage("startTimePoint", inputTime, "AddFirst");
        tree.registerIncomingStage(startTimePoint, "static");

        ResponseInterceptor outputTime = new OutputTimeInterceptor();
        OutcomingPipelineStage endTimePoint = new OutcomingPipelineStage("endTimePoint", outputTime, "AddFirst");
        tree.registerOutcomingStage(endTimePoint, "static");
    }

    public String getName() {
        return name;
    }
}
